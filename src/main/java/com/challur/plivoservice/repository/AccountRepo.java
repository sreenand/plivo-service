package com.challur.plivoservice.repository;

import com.challur.plivoservice.models.Account;

/**
 * Created by srinand.challur on 7/11/17.
 */
public interface AccountRepo {

	Account getAccount(String username, String password);
}
