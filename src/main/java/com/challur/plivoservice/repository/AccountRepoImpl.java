package com.challur.plivoservice.repository;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.challur.plivoservice.models.Account;
import io.dropwizard.hibernate.AbstractDAO;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * Created by srinand.challur on 7/11/17.
 */


public class AccountRepoImpl extends AbstractDAO<Account> implements AccountRepo {

	private SessionFactory sessionFactory;


	public AccountRepoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
		this.sessionFactory = sessionFactory;
	}

	public Account getAccount(String username, String password) {
		List<Account> accounts =  list(currentSession().createCriteria(Account.class).
				add(Restrictions.eq("username", username)).add(Restrictions.eq("authId",password)));

		if(accounts != null & accounts.size() > 0) {
			return accounts.get(0);
		} else {
			return null;
		}
	}
}
