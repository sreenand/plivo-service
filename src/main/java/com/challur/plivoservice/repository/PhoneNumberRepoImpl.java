package com.challur.plivoservice.repository;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.challur.plivoservice.models.Account;
import com.challur.plivoservice.models.PhoneNumber;

import io.dropwizard.hibernate.AbstractDAO;

/**
 * Created by srinand.challur on 7/18/17.
 */
public class PhoneNumberRepoImpl extends AbstractDAO<PhoneNumber> implements PhoneNumberRepo {


	public PhoneNumberRepoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public PhoneNumber find(String to, int accountId) {
		List<PhoneNumber> phoneNumberList =  list(currentSession().createCriteria(PhoneNumber.class).
				add(Restrictions.eq("number", to)).add(Restrictions.eq("accountId",new Integer(accountId))));
		if(phoneNumberList != null && phoneNumberList.size() > 0){
			return phoneNumberList.get(0);
		}
		return null;
	}
}
