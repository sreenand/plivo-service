package com.challur.plivoservice.repository;

import com.challur.plivoservice.models.PhoneNumber;

/**
 * Created by srinand.challur on 7/17/17.
 */
public interface PhoneNumberRepo {

	public PhoneNumber find(String to, int accountId);
}
