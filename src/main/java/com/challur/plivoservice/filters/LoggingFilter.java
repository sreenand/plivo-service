package com.challur.plivoservice.filters;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;


import javax.ws.rs.container.ContainerRequestFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.NoArgsConstructor;

/**
 * Created by srinand.challur on 7/19/17.
 */

@Provider
@NoArgsConstructor
public class LoggingFilter implements ContainerRequestFilter, ContainerResponseFilter {

	private static final Logger logger = LoggerFactory.getLogger(LoggingFilter.class);
	public void filter(ContainerRequestContext containerRequestContext) throws IOException {
		logger.error("in request filter");
	}

	public void filter(ContainerRequestContext containerRequestContext, ContainerResponseContext containerResponseContext) throws IOException {
		logger.error("in response filter");

	}
}
