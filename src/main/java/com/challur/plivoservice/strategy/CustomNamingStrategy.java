package com.challur.plivoservice.strategy;

import org.hibernate.cfg.ImprovedNamingStrategy;

/**
 * Created by srinand.challur on 7/11/17.
 */
public class CustomNamingStrategy extends ImprovedNamingStrategy {
	private static final String PLURAL_SUFFIX = "";

	/**
	 * Transforms class names to table names by using the described naming conventions.
	 * @param className
	 * @return  The constructed table name.
	 */
	@Override
	public String classToTableName(String className) {
		String tableNameInSingularForm = super.classToTableName(className);
		return transformToPluralForm(tableNameInSingularForm);
	}

	private String transformToPluralForm(String tableNameInSingularForm) {
		StringBuilder pluralForm = new StringBuilder();

		pluralForm.append(tableNameInSingularForm);
		pluralForm.append(PLURAL_SUFFIX);

		return pluralForm.toString();
	}
}
