package com.challur.plivoservice;

import org.hibernate.engine.transaction.synchronization.spi.ExceptionMapper;

import com.challur.plivoservice.configuration.PlivoServiceConfiguration;
import com.challur.plivoservice.errors.AppException;
import com.challur.plivoservice.errors.AppExceptionMapper;
import com.challur.plivoservice.errors.UnknownExceptionMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import io.dropwizard.jetty.ConnectorFactory;
import io.dropwizard.jetty.HttpConnectorFactory;
import io.dropwizard.server.DefaultServerFactory;

/**
 * Created by srinand.challur on 7/11/17.
 */
public class PlivoServiceModule extends AbstractModule {

	@Override
	protected void configure() {

	}


	@Provides @Singleton
	public ObjectMapper provideMapper() {
		return new ObjectMapper();
	}


	@Provides
	public HttpConnectorFactory provideServerConfig(PlivoServiceConfiguration configuration)  {
		HttpConnectorFactory httpConnector = null;
		DefaultServerFactory serverFactory = (DefaultServerFactory) configuration.getServerFactory();
		for (ConnectorFactory connector : serverFactory.getApplicationConnectors()) {
			if (connector.getClass().isAssignableFrom(HttpConnectorFactory.class)) {
				httpConnector = (HttpConnectorFactory) connector;
			}
		}
		return httpConnector;
	}

	@Provides
	public javax.ws.rs.ext.ExceptionMapper<AppException> getAppExceptionMapper(){
		return new AppExceptionMapper();
	}

	@Provides
	public javax.ws.rs.ext.ExceptionMapper<RuntimeException> getRunTimeExceptionMapper(){
		return new UnknownExceptionMapper();
	}

}
