package com.challur.plivoservice.resources;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.challur.plivoservice.Response.StandardResponse;
import com.challur.plivoservice.Utils.PhoneResourceUtils;
import com.challur.plivoservice.errors.AppException;
import com.challur.plivoservice.models.Account;
import com.challur.plivoservice.models.PhoneNumber;
import com.challur.plivoservice.repository.AccountRepo;
import com.challur.plivoservice.repository.PhoneNumberRepo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.jedis.lock.JedisLock;
import com.google.common.base.Strings;
import com.google.inject.Inject;

import io.dropwizard.hibernate.UnitOfWork;
import lombok.Getter;
import lombok.Setter;
import redis.clients.jedis.Jedis;

/**
 * Created by srinand.challur on 7/11/17.
 */

@Path("/")

@Getter @Setter
public class PhoneResource {


	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PhoneResource.class);
	private AccountRepo accountRepo;
	private ObjectMapper objectMapper;
	private PhoneResourceUtils phoneResourceUtils;
	private PhoneNumberRepo phoneNumberRepo;
	private Jedis jedis;
	private JedisLock jedisLock;
	public static final String STOP = "STOP";
	public static final String COUNT = "COUNT";
	public static final String JEDIS_LOCK = "JEDIS_LOCK_";

	@Inject
	public 	PhoneResource(AccountRepo accountRepo, ObjectMapper objectMapper, PhoneResourceUtils phoneResourceUtils, PhoneNumberRepo phoneNumberRepo, Jedis jedis){
		this.accountRepo = accountRepo;
		this.objectMapper = objectMapper;
		this.phoneResourceUtils = phoneResourceUtils;
		this.phoneNumberRepo = phoneNumberRepo;
		this.jedis = jedis;
	}

	@POST @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	@Path("/inbound/sms")
	public Response inboundSMS(Map<String,String> parameters) {

		String username = parameters.get("username");
		String password = parameters.get("password");

		Account account = accountRepo.getAccount(username,password);
		if(!phoneResourceUtils.authenticate(username,password,account)){
			return Response.status(Response.Status.FORBIDDEN).entity(new StandardResponse("Authentication failed","")).build();
		}

		String from = parameters.get("from");
		String to = parameters.get("to");
		String text = parameters.get("text");

		AppException exception = phoneResourceUtils.checkMissing(from,to,text);

		if(exception != null){
			throw exception;
		}

		exception = phoneResourceUtils.checkValid(from,to,text);

		if(exception != null){
			throw exception;
		}
		PhoneNumber phoneNumber = phoneNumberRepo.find(to,account.getId());

		if(phoneNumber == null){
			exception = new AppException(Response.Status.BAD_REQUEST.getStatusCode(),"to parameter not found");
			throw exception;
		}

		phoneResourceUtils.putStop(jedis, from, to, text);

		StandardResponse response = new StandardResponse("","inbound sms ok");
		return Response.status(Response.Status.OK).entity(response).build();

	}

	@POST @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	@Path("/outbound/sms")
	public Response outboundSMS(Map<String,String> parameters) {

		String username = parameters.get("username");
		String password = parameters.get("password");

		Account account = accountRepo.getAccount(username,password);

		if(!phoneResourceUtils.authenticate(username,password,account)){
			return Response.status(Response.Status.FORBIDDEN).build();
		}

		String from = parameters.get("from");
		String to = parameters.get("to");
		String text = parameters.get("text");

		AppException exception = phoneResourceUtils.checkMissing(from,to,text);

		if(exception != null){
			throw exception;
		}

		exception = phoneResourceUtils.checkValid(from,to,text);
		if(exception != null){
			throw exception;
		}
		PhoneNumber phoneNumber = phoneNumberRepo.find(from,account.getId());
		if(phoneNumber == null){
			exception = new AppException(Response.Status.BAD_REQUEST.getStatusCode(),"from parameter not found");
			throw exception;
		}

		try {
			jedisLock = phoneResourceUtils.aquireLock(jedis,from);
			if(jedisLock == null){
				throw new AppException(Response.Status.PRECONDITION_FAILED.getStatusCode(),"cannot send two sms at same time for "+from);
			}
			exception = phoneResourceUtils.checkSTOP(jedis, from, to);
			if (exception != null) {
				throw exception;
			}
			exception = phoneResourceUtils.checkInCache(jedis, from);
			if (exception != null) {
				throw exception;
			}
			phoneResourceUtils.increaseCount(jedis,from);
		} finally {
			if(jedisLock != null) {
				jedisLock.release();
			}
		}

		StandardResponse response = new StandardResponse("","outbound sms ok");
		return Response.status(Response.Status.OK).entity(response).build();
	}
}
