package com.challur.plivoservice.Utils;

import static com.challur.plivoservice.resources.PhoneResource.JEDIS_LOCK;

import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.challur.plivoservice.errors.AppException;
import com.challur.plivoservice.models.Account;
import com.challur.plivoservice.repository.AccountRepo;
import com.challur.plivoservice.resources.PhoneResource;
import com.github.jedis.lock.JedisLock;
import com.google.common.base.Strings;

import lombok.NoArgsConstructor;
import redis.clients.jedis.Jedis;

/**
 * Created by srinand.challur on 7/17/17.
 */

@NoArgsConstructor
public class PhoneResourceUtils {

	Logger logger = LoggerFactory.getLogger(PhoneResourceUtils.class);


	public boolean authenticate(String username, String password, Account account){

		if(Strings.isNullOrEmpty(username) || account == null || !account.getUsername().equals(username) || !account.getAuthId().equals(password)){
			return false;
		}
		return true;
	}

	public AppException checkMissing(String from, String to, String text){
		if(from == null){
			return new AppException(Response.Status.BAD_REQUEST.getStatusCode(),"from parameter is missing");
		}
		if(to == null){
			return new AppException(Response.Status.BAD_REQUEST.getStatusCode(),"to parameter is missing");
		}
		return null;
	}

	public AppException checkValid(String from , String to , String text){

		if(to.length() < 6 || to.length() > 16){
			return new AppException(Response.Status.BAD_REQUEST.getStatusCode(),"from parameter is invalid");
		}
		if(from.length() < 6 || from.length() > 16){
			return new AppException(Response.Status.BAD_REQUEST.getStatusCode(),"to parameter is invalid");
		}
		if(text == null || text.length() < 1){
			return new AppException(Response.Status.BAD_REQUEST.getStatusCode(),"invalid text");
		}
		return null;
	}


	public void putStop(Jedis jedis, String from , String to, String text){

		if(text.equals("STOP") || text.equals("STOP\n") || text.equals("STOP\r") || text.equals("STOP\n\r") && !Strings.isNullOrEmpty(from) && !Strings.isNullOrEmpty(to)){
			jedis.set(PhoneResource.STOP+"_"+from+"_"+to,"STOP");
			jedis.expire(PhoneResource.STOP+"_"+from+"_"+to, 14400);
		}
	}

	public AppException checkSTOP(Jedis jedis, String from, String to){

		if(Strings.isNullOrEmpty(from) || Strings.isNullOrEmpty(to) || jedis.get(PhoneResource.STOP+"_"+to+"_"+from) != null){
			return new AppException(Response.Status.NOT_ACCEPTABLE.getStatusCode(),"sms from "+from+" to "+to+" blocked by STOP request");
		}
		return null;
	}

	public void increaseCount(Jedis jedis, String from){

		String count = jedis.get(PhoneResource.COUNT+"_"+from);
		int intCount = 0;
		try{
			intCount = Integer.parseInt(count);
		}catch (Exception e){
			intCount = 0;
		}
		intCount++;
		jedis.set(PhoneResource.COUNT+"_"+from,""+intCount);

		if(intCount == 1){
			jedis.expire(PhoneResource.COUNT+"_"+from,86400);
		}

	}

	public AppException checkInCache(Jedis jedis, String from){
		String count = jedis.get(PhoneResource.COUNT+"_"+from);
		int intCount = 0;
		try{
			intCount = Integer.parseInt(count);
		}catch (Exception e){
			intCount = 0;
		}

		if(intCount >= 50){
			return new AppException(Response.Status.NOT_ACCEPTABLE.getStatusCode(),"limit reached for from "+from);
		}
		return  null;
	}

	public JedisLock aquireLock(Jedis jedis, String from){
		JedisLock jedisLock = new JedisLock(jedis,JEDIS_LOCK+from);
		try {
			if(jedisLock.acquire()){
				return jedisLock;
			}
		} catch (Exception e){
			return null;
		}
		return null;

	}



}
