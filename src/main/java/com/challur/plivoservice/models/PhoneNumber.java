package com.challur.plivoservice.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by srinand.challur on 7/17/17.
 */

@Entity @Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class PhoneNumber {

	@Id @GeneratedValue
	private int id;
	private String number;
	private int accountId;

}
