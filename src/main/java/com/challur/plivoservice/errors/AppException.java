package com.challur.plivoservice.errors;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by srinand.challur on 7/18/17.
 */

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class AppException extends RuntimeException {

	private static final long serialVersionUID = -8999932578270387947L;
	private Integer status;
	private String developerMessage;
}
