package com.challur.plivoservice.errors;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import com.challur.plivoservice.Response.StandardResponse;

import io.dropwizard.jersey.errors.ErrorMessage;

/**
 * Created by srinand.challur on 7/18/17.
 */
public class AppExceptionMapper implements ExceptionMapper<AppException> {

	public Response toResponse(AppException ex) {

		StandardResponse response = new StandardResponse(ex.getDeveloperMessage(),"");
		return Response.status(ex.getStatus())
				.entity(new ErrorMessage(ex.getMessage()))
				.type(MediaType.APPLICATION_JSON).entity(response).
						build();
	}
}
