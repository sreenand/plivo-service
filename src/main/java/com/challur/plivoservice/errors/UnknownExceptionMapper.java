package com.challur.plivoservice.errors;


import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import com.challur.plivoservice.Response.StandardResponse;

import io.dropwizard.jersey.errors.ErrorMessage;

/**
 * Created by srinand.challur on 7/22/17.
 */
public class UnknownExceptionMapper implements ExceptionMapper<RuntimeException> {

	public Response toResponse(RuntimeException e) {
		StandardResponse response = new StandardResponse("Unknown Failure","");
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
				.type(MediaType.APPLICATION_JSON).entity(response).
						build();
	}
}
