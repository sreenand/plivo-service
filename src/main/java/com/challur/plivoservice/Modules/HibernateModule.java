package com.challur.plivoservice.Modules;

import com.challur.plivoservice.configuration.PlivoServiceConfiguration;
import com.challur.plivoservice.repository.AccountRepo;
import com.challur.plivoservice.repository.AccountRepoImpl;
import com.challur.plivoservice.repository.PhoneNumberRepo;
import com.challur.plivoservice.repository.PhoneNumberRepoImpl;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;

import io.dropwizard.hibernate.HibernateBundle;

/**
 * Created by srinand.challur on 7/11/17.
 */
public class HibernateModule extends AbstractModule {

	private HibernateBundle<PlivoServiceConfiguration> hibernateBundle;

	public HibernateModule(HibernateBundle<PlivoServiceConfiguration> hibernate) {
		this.hibernateBundle = hibernate;
	}


	@Override
	protected void configure() {

	}

	@Provides
	public AccountRepo provideAccountDAO(){
		return new AccountRepoImpl(hibernateBundle.getSessionFactory());
	}

	@Provides
	public PhoneNumberRepo providePhoneNumberDAO(){
		return new PhoneNumberRepoImpl(hibernateBundle.getSessionFactory());
	}
}
