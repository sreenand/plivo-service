package com.challur.plivoservice.Modules;

import com.bendb.dropwizard.redis.JedisBundle;
import com.bendb.dropwizard.redis.JedisFactory;
import com.challur.plivoservice.configuration.PlivoServiceConfiguration;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * Created by srinand.challur on 7/19/17.
 */
public class JedisModule extends AbstractModule {


	private JedisBundle<PlivoServiceConfiguration> jedisBundle;

	public JedisModule(JedisBundle<PlivoServiceConfiguration> jedisBundle){
		this.jedisBundle = jedisBundle;
	}

	@Override
	protected void configure() {

	}

	@Provides
	public JedisPool provideJedisPool(){
		return jedisBundle.getPool();
	}

	@Provides
	public Jedis provideJedis(){
		return jedisBundle.getPool().getResource();
	}
}
