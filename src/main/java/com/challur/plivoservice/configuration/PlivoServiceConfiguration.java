package com.challur.plivoservice.configuration;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.bendb.dropwizard.redis.JedisFactory;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by srinand.challur on 7/11/17.
 */

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class PlivoServiceConfiguration extends Configuration {

	@Valid
	@NotNull
	private DataSourceFactory database;

	@Valid
	@NotNull
	private JedisFactory redis;


}
