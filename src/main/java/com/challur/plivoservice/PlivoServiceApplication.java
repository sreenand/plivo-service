package com.challur.plivoservice;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;

import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.hibernate.cfg.Configuration;

import com.bendb.dropwizard.redis.JedisBundle;
import com.bendb.dropwizard.redis.JedisFactory;
import com.challur.plivoservice.Modules.HibernateModule;
import com.challur.plivoservice.Modules.JedisModule;
import com.challur.plivoservice.configuration.PlivoServiceConfiguration;
import com.challur.plivoservice.errors.AppExceptionMapper;
import com.challur.plivoservice.errors.UnknownExceptionMapper;
import com.challur.plivoservice.filters.LoggingFilter;
import com.challur.plivoservice.models.Account;
import com.challur.plivoservice.models.PhoneNumber;
import com.challur.plivoservice.strategy.CustomNamingStrategy;
import com.google.inject.Stage;
import com.hubspot.dropwizard.guice.GuiceBundle;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import java.util.EnumSet;

/**
 * Created by srinand.challur on 7/11/17.
 */
public class PlivoServiceApplication extends Application<PlivoServiceConfiguration> {

	private final HibernateBundle<PlivoServiceConfiguration> hibernate = new HibernateBundle<PlivoServiceConfiguration>(
			Account.class, PhoneNumber.class) {

		public DataSourceFactory getDataSourceFactory(PlivoServiceConfiguration configuration) {
			return configuration.getDatabase();
		}

		@Override
		public void configure(Configuration configuration) {
			configuration.setNamingStrategy(new CustomNamingStrategy());
			configuration.addPackage("com.challur.plivoservice.models");
		}
	};

	private final JedisBundle<PlivoServiceConfiguration> redisBundle = new JedisBundle<PlivoServiceConfiguration>() {

		public JedisFactory getJedisFactory(PlivoServiceConfiguration configuration) {
			return configuration.getRedis();
		}
	};

	@Override
	public void initialize(Bootstrap<PlivoServiceConfiguration> bootstrap) {

		GuiceBundle<PlivoServiceConfiguration> guiceBundle = GuiceBundle.<PlivoServiceConfiguration>newBuilder()
				.addModule(new PlivoServiceModule()).addModule(new HibernateModule(hibernate))
				.enableAutoConfig(getClass().getPackage().getName())
				.setConfigClass(PlivoServiceConfiguration.class)
				.build(Stage.DEVELOPMENT);

		bootstrap.addBundle(new AssetsBundle("/public", "/assets"));
		bootstrap.addBundle(hibernate);
		bootstrap.addBundle(guiceBundle);

	}

	public void run(PlivoServiceConfiguration plivoServiceConfiguration, Environment environment) throws Exception {

		final FilterRegistration.Dynamic cors =
				environment.servlets().addFilter("CORS", CrossOriginFilter.class);

		// Configure CORS parameters
		cors.setInitParameter("allowedOrigins", "*");
		cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin");
		cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");
		cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
		environment.jersey().register(new AppExceptionMapper());
		environment.jersey().register(new UnknownExceptionMapper());
		environment.jersey().register(LoggingFilter.class);

	}



	public static void main(String[] args) throws Exception {
		new PlivoServiceApplication().run(args);
	}


}
