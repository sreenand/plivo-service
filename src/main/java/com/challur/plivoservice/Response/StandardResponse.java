package com.challur.plivoservice.Response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by srinand.challur on 7/18/17.
 */

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class StandardResponse {

	private String error;
	private String message;
}
