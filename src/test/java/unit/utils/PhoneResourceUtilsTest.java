package unit.utils;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;

import com.challur.plivoservice.Utils.PhoneResourceUtils;
import com.challur.plivoservice.errors.AppException;
import com.challur.plivoservice.models.Account;
import com.challur.plivoservice.resources.PhoneResource;

import redis.clients.jedis.Jedis;

/**
 * Created by srinand.challur on 7/20/17.
 */
public class PhoneResourceUtilsTest {



	private Jedis jedis = mock(Jedis.class);
	private PhoneResourceUtils phoneResourceUtils = new PhoneResourceUtils();
	private Account account;

	private String username1 = "username1";
	private String username2 = "username2";
	private String username3 = "username3";

	private String password1 = "password1";
	private String password2 = "password2";
	private String password3 = "password3";


	@Before
	public void setUp(){
		initMocks(this);
	}

	@Test
	public void testAuthenticateNormalFlow(){
		this.account = new Account(1,password1,username1);
		assert(phoneResourceUtils.authenticate(username1,password1,this.account));
	}

	@Test
	public void testAuthenticateFalse(){
		this.account = new Account(1,password2,username1);
		assert(!phoneResourceUtils.authenticate(username1,password1,this.account));
	}


	@Test
	public void testAuthenticateUsernameNull(){
		this.account = new Account(1,password2,username1);
		assert(!phoneResourceUtils.authenticate(null,password1,this.account));
	}


	@Test
	public void testAuthenticatePasswordNull(){
		this.account = new Account(1,password1,username1);
		assert(!phoneResourceUtils.authenticate(username1,null,this.account));
	}

	@Test
	public void testAuthenticateAccountNull(){
		this.account = new Account(1,password1,username1);
		assert(!phoneResourceUtils.authenticate(username1,password1,null));
	}

	@Test
	public void checkMissingNormalFlow(){
		assert(phoneResourceUtils.checkMissing("from","to","text") == null);
	}

	@Test
	public void checkMissingMissingFlow(){
		assert(phoneResourceUtils.checkMissing(null,"to","text") != null);
		assert(phoneResourceUtils.checkMissing("from",null,"text") != null);
	}

	@Test
	public void checkMissingMissingFlowException(){

		AppException exception1 = phoneResourceUtils.checkMissing(null,"to","text");
		AppException exception2 = phoneResourceUtils.checkMissing("from",null,"text");

		assert(exception1.getStatus().equals(Response.Status.BAD_REQUEST.getStatusCode()));
		assert(exception2.getStatus().equals(Response.Status.BAD_REQUEST.getStatusCode()));

		exception1.getDeveloperMessage().equals("from parameter is missing");
		exception2.getDeveloperMessage().equals("to parameter is missing");
	}

	@Test
	public void checkValidNormalFlow(){
		assert(phoneResourceUtils.checkMissing("8553604772","832843648346","some text") == null);
	}

	@Test
	public void checkValidFlowException(){

		AppException exception1 = phoneResourceUtils.checkValid("","832843648346","text");
		AppException exception2 = phoneResourceUtils.checkValid("832843648346","","text");
		AppException exception3 = phoneResourceUtils.checkValid("832843648346","832843648346","");

		assert (exception1.getStatus().equals(Response.Status.BAD_REQUEST.getStatusCode()));
		assert (exception2.getStatus().equals(Response.Status.BAD_REQUEST.getStatusCode()));
		assert (exception3.getStatus().equals(Response.Status.BAD_REQUEST.getStatusCode()));

		exception1.getDeveloperMessage().equals("from parameter is invalid");
		exception2.getDeveloperMessage().equals("to parameter is invalid");
		exception3.getDeveloperMessage().equals("invalid text");
	}

	@Test
	public void putStopTest(){

		String from = "8553604772";
		String to = "832843648346";

		phoneResourceUtils.putStop(jedis, from,to,"STOP");
		phoneResourceUtils.putStop(jedis, from,to,"STOP\n");
		phoneResourceUtils.putStop(jedis, from,to,"STOP\r");
		phoneResourceUtils.putStop(jedis, from,to,"STOP\n\r");
		phoneResourceUtils.putStop(jedis, from,to,"STOPDFSDFSDF\n\r");
		phoneResourceUtils.putStop(jedis, from,to,"STOPDFSDFSDFDFDFD\n\r");
		phoneResourceUtils.putStop(jedis, null,null,"STOPDFSDFSDF\n\r");
		phoneResourceUtils.putStop(jedis, null,to,"STOPDFSDFSDFDFDFD\n\r");
		phoneResourceUtils.putStop(jedis, from,null,"STOPDFSDFSDF\n\r");
		phoneResourceUtils.putStop(jedis, "","","STOPDFSDFSDFDFDFD\n\r");

		verify(jedis,times(4)).set(PhoneResource.STOP+"_"+from+"_"+to,PhoneResource.STOP);
		verify(jedis,times(4)).expire(PhoneResource.STOP+"_"+from+"_"+to,14400);


	}

	@Test
	public void checkStopTest(){

		String from = "8553604772";
		String to = "832843648346";

		when(jedis.get(anyString())).thenReturn("STOP");

		AppException appException = phoneResourceUtils.checkSTOP(jedis,from,to);
		assert(appException != null);
		assert(appException.getDeveloperMessage().equals("sms from 8553604772 to 832843648346 blocked by STOP request"));
		assert(appException.getStatus().equals(Response.Status.NOT_ACCEPTABLE.getStatusCode()));

	}

	@Test
	public void checkInCacheTest(){
		String from = "8553604772";
		String to = "832843648346";

		when(jedis.get(PhoneResource.COUNT+"_"+from)).thenReturn("50");

		AppException appException = phoneResourceUtils.checkInCache(jedis,from);
		assert(appException != null);
		assert(appException.getDeveloperMessage().equals("limit reached for from 8553604772"));
		assert(appException.getStatus().equals(Response.Status.NOT_ACCEPTABLE.getStatusCode()));

	}
}
