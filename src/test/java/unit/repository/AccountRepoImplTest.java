package unit.repository;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.challur.plivoservice.models.Account;
import com.challur.plivoservice.models.PhoneNumber;
import com.challur.plivoservice.repository.AccountRepoImpl;

/**
 * Created by srinand.challur on 7/20/17.
 */
public class AccountRepoImplTest {


	private SessionFactory sessionFactory = mock(SessionFactory.class);
	private Session session = mock(Session.class);
	private AccountRepoImpl accountRepo = new AccountRepoImpl(sessionFactory);
	private Criteria criteria = mock(Criteria.class);

	private Account account = new Account(1,"ABCD","PLIVOTEST");
	private List<Account> queryList = new ArrayList<Account>();



	@Before
	public void setup() throws Exception {
		initMocks(this);
		when(sessionFactory.getCurrentSession()).thenReturn(session);
		when(session.createCriteria(Account.class)).thenReturn(criteria);

	}

	@Test
	public void getAccountTest(){

		when(criteria.add((SimpleExpression)anyObject())).thenReturn(criteria);
		when(criteria.add((SimpleExpression)anyObject())).thenReturn(criteria);

		queryList.add(account);
		when(criteria.list()).thenReturn(queryList);
		Account account = accountRepo.getAccount("PLIVOTEST","ABCD");

		verify(criteria,atMost(1)).add(Restrictions.eq("username","PLIVOTEST"));
		verify(criteria,atMost(1)).add(Restrictions.eq("authId","ADCD"));

		assert(account.getAuthId().equals(this.account.getAuthId()));
		assert(account.getUsername().equals(this.account.getUsername()));
		assert(account.getId() == (this.account.getId()));
	}

	@Test
	public void getAccountTestForNull(){

		when(criteria.add((SimpleExpression)anyObject())).thenReturn(criteria);
		when(criteria.add((SimpleExpression)anyObject())).thenReturn(criteria);

		Account account = accountRepo.getAccount("PLIVOTEST","ABCD");

		verify(criteria,atMost(1)).add(Restrictions.eq("username","PLIVOTEST"));
		verify(criteria,atMost(1)).add(Restrictions.eq("authId","ADCD"));

		assert(account == null);
	}




}
