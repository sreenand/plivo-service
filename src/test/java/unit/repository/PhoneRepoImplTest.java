package unit.repository;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.junit.Before;
import org.junit.Test;

import com.challur.plivoservice.models.Account;
import com.challur.plivoservice.models.PhoneNumber;
import com.challur.plivoservice.repository.PhoneNumberRepo;
import com.challur.plivoservice.repository.PhoneNumberRepoImpl;

/**
 * Created by srinand.challur on 7/20/17.
 */
public class PhoneRepoImplTest {


	private Session session = mock(Session.class);
	private Criteria criteria = mock(Criteria.class);
	private SessionFactory sessionFactory = mock(SessionFactory.class);
	private PhoneNumberRepoImpl phoneNumberRepo = new PhoneNumberRepoImpl(sessionFactory);
	private List<PhoneNumber> phoneNumberList = new ArrayList<PhoneNumber>();
	private PhoneNumber phoneNumber = new PhoneNumber(1,"8553604772",1);

	@Before
	public void setup() throws Exception {
		initMocks(this);
		when(sessionFactory.getCurrentSession()).thenReturn(session);
		when(session.createCriteria(PhoneNumber.class)).thenReturn(criteria);

	}



	@Test
	public void getPhoneNumberTest(){

		when(criteria.add((SimpleExpression)anyObject())).thenReturn(criteria);
		when(criteria.add((SimpleExpression)anyObject())).thenReturn(criteria);

		phoneNumberList.add(phoneNumber);
		when(criteria.list()).thenReturn(phoneNumberList);
		PhoneNumber phoneNumber = phoneNumberRepo.find("8553604772",1);

		verify(criteria,atMost(1)).add(Restrictions.eq("accountId",new Integer(1)));
		verify(criteria,atMost(1)).add(Restrictions.eq("number","8553604772"));


	}


	@Test
	public void getPhoneNumberTestNull(){

		when(criteria.add((SimpleExpression)anyObject())).thenReturn(criteria);
		when(criteria.add((SimpleExpression)anyObject())).thenReturn(criteria);

		PhoneNumber phoneNumber = phoneNumberRepo.find("8553604772",1);

		verify(criteria,atMost(1)).add(Restrictions.eq("accountId",new Integer(1)));
		verify(criteria,atMost(1)).add(Restrictions.eq("number","8553604772"));

		assert(phoneNumber == null);

	}


}
