package unit.resource;



import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.test.grizzly.GrizzlyWebTestContainerFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;


import com.challur.plivoservice.Utils.PhoneResourceUtils;
import com.challur.plivoservice.errors.AppException;
import com.challur.plivoservice.models.Account;
import com.challur.plivoservice.models.PhoneNumber;
import com.challur.plivoservice.repository.AccountRepo;
import com.challur.plivoservice.repository.PhoneNumberRepo;
import com.challur.plivoservice.resources.PhoneResource;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.jedis.lock.JedisLock;

import io.dropwizard.testing.junit.ResourceTestRule;
import redis.clients.jedis.Jedis;

/**
 * Created by srinand.challur on 7/22/17.
 */
public class PhoneResourceTest {


	private static PhoneResourceUtils phoneResourceUtils = mock(PhoneResourceUtils.class);
	private static ObjectMapper objectMapper = new ObjectMapper();
	private static PhoneNumberRepo phoneNumberRepo = mock(PhoneNumberRepo.class);
	private static AccountRepo accountRepo = mock(AccountRepo.class);
	private static Jedis jedis = mock(Jedis.class);
	private JedisLock jedisLock = mock(JedisLock.class);
	private Account account;


	@ClassRule
	public static final ResourceTestRule rule = ResourceTestRule.builder().setTestContainerFactory(new GrizzlyWebTestContainerFactory())
			.addResource(new PhoneResource(accountRepo,objectMapper,phoneResourceUtils,phoneNumberRepo,jedis)).build();

	@Before
	public void setUp(){
		initMocks(this);
	}

	@Test
	public void  testNormalFlowInboundSMS(){

		Map<String,String> payload = new HashMap<String, String>();
		payload.put("from","8553604772");
		payload.put("to","85536047723434");
		payload.put("text","some text");
		payload.put("username","plivo1");
		payload.put("password","password");

		account = new Account(1,"password","plivo1");
		when(accountRepo.getAccount("plivo1","password")).thenReturn(account);
		when(phoneResourceUtils.authenticate("plivo1","password",account)).thenReturn(true);
		when(phoneResourceUtils.checkValid("8553604772","85536047723434","some text")).thenReturn(null);
		when(phoneResourceUtils.checkMissing("8553604772","85536047723434","some text")).thenReturn(null);
		when(phoneNumberRepo.find("85536047723434",account.getId())).thenReturn(new PhoneNumber());

		Response response = null;
		try {
			 response= rule.getJerseyTest().target("/inbound/sms").request().post(Entity.entity(objectMapper.writeValueAsString(payload),MediaType.APPLICATION_JSON_TYPE));
		} catch (Exception e) {
			e.printStackTrace();
		}
		assert(response.getStatus() == Response.Status.OK.getStatusCode());

	}

	@Test
	public void  testVerifyStop(){

		Map<String,String> payload = new HashMap<String, String>();
		payload.put("from","8553604772");
		payload.put("to","85536047723434");
		payload.put("text","STOP");
		payload.put("username","plivo1");
		payload.put("password","password");

		account = new Account(1,"password","plivo1");
		when(accountRepo.getAccount("plivo1","password")).thenReturn(account);
		when(phoneResourceUtils.authenticate("plivo1","password",account)).thenReturn(true);
		when(phoneResourceUtils.checkValid("8553604772","85536047723434","some text")).thenReturn(null);
		when(phoneResourceUtils.checkMissing("8553604772","8553604772","some text")).thenReturn(null);
		when(phoneNumberRepo.find("85536047723434",account.getId())).thenReturn(new PhoneNumber());


		Response response = null;
		try {
			response= rule.getJerseyTest().target("/inbound/sms").request().post(Entity.entity(objectMapper.writeValueAsString(payload),MediaType.APPLICATION_JSON_TYPE));
		} catch (Exception e) {
			e.printStackTrace();
		}
		assert(response.getStatus() == Response.Status.OK.getStatusCode());
	}

	@Test
	public void  verifyAuthenticateinboundSMS() throws Exception{

		Map<String,String> payload = new HashMap<String, String>();
		payload.put("from","8553604772");
		payload.put("to","85536047723434");
		payload.put("text","some text");
		payload.put("username","plivo1");
		payload.put("password","password");
		account = new Account(1,"password","username");
		when(accountRepo.getAccount("plivo1","password")).thenReturn(account);
		Response response = rule.getJerseyTest().target("/inbound/sms").request().post(Entity.entity(objectMapper.writeValueAsString(payload),MediaType.APPLICATION_JSON_TYPE));
		assert response.getStatus() == 403;


	}

	@Test
	public void  verifyValidinboundSMS() throws Exception{

		Map<String,String> payload = new HashMap<String, String>();
		payload.put("username","plivo1");
		payload.put("password","password");
		payload.put("from","8553604772");
		payload.put("to","8553645");
		payload.put("text","some text");
		account = new Account(1,"password","plivo1");
		when(accountRepo.getAccount("plivo1","password")).thenReturn(account);
		when(phoneResourceUtils.authenticate("plivo1","password",account)).thenReturn(true);
		when(phoneResourceUtils.checkMissing("8553604772","8553645","some text")).thenReturn(null);
		when(phoneResourceUtils.checkValid("8553604772","8553645","some text")).thenReturn(null);
		when(phoneNumberRepo.find("8553645",account.getId())).thenReturn(new PhoneNumber());
		Response response = rule.getJerseyTest().target("/inbound/sms").request().post(Entity.entity(objectMapper.writeValueAsString(payload),MediaType.APPLICATION_JSON_TYPE));
		assert response.getStatus() == Response.Status.OK.getStatusCode();


	}

	@Test
	public void  verifyMissingInboundSMS() throws Exception{

		Map<String,String> payload = new HashMap<String, String>();
		payload.put("username","plivo1");
		payload.put("password","password");
		payload.put("from","8553604772");
		payload.put("text","some text");
		account = new Account(1,"password","plivo1");
		when(accountRepo.getAccount("plivo1","password")).thenReturn(account);
		when(phoneResourceUtils.authenticate("plivo1","password",account)).thenReturn(true);
		when(phoneResourceUtils.checkMissing(anyString(),anyString(),anyString())).thenReturn(new AppException(400,"to parameter missing"));
		Response response = rule.getJerseyTest().target("/inbound/sms").request().post(Entity.entity(objectMapper.writeValueAsString(payload),MediaType.APPLICATION_JSON_TYPE));
		assert response.getStatus() == Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();


	}

	@Test
	public void  testNormalFlowOutboundSMS() throws InterruptedException {

		Map<String,String> payload = new HashMap<String, String>();
		payload.put("to","8553604772");
		payload.put("from","85536047723434");
		payload.put("text","some text");
		payload.put("username","plivo1");
		payload.put("password","password");

		account = new Account(1,"password","username");
		when(accountRepo.getAccount("plivo1","password")).thenReturn(account);
		when(phoneResourceUtils.authenticate("plivo1","password",account)).thenReturn(true);
		when(phoneResourceUtils.checkValid("from","to","some text")).thenReturn(null);
		when(phoneResourceUtils.checkMissing("from","to","some text")).thenReturn(null);
		when(phoneResourceUtils.aquireLock(jedis,"85536047723434")).thenReturn(jedisLock);
		when(phoneNumberRepo.find("85536047723434",account.getId())).thenReturn(new PhoneNumber());

		Map<String, String> response = null;
		try {
			response = rule.getJerseyTest().target("/outbound/sms").request().post(Entity.entity(objectMapper.writeValueAsString(payload),MediaType.APPLICATION_JSON_TYPE),Map.class);
		} catch (Exception e) {
			e.printStackTrace();
		}

		verify(jedisLock).release();
		assert(response.get("error").equals(""));
		assert(response.get("message").equals("outbound sms ok"));
	}


	@Test
	public void  testAuthenticateOutboundSMS() throws Exception {

		Map<String, String> payload = new HashMap<String, String>();
		payload.put("to", "8553604772");
		payload.put("from", "85536047723434");
		payload.put("text", "some text");
		payload.put("username", "plivo1");
		payload.put("password", "password");
		when(accountRepo.getAccount("plivo1","password")).thenReturn(null);
		Response response = rule.getJerseyTest().target("/outbound/sms").request().post(Entity.entity(objectMapper.writeValueAsString(payload),MediaType.APPLICATION_JSON_TYPE));
		assert response.getStatus() == Response.Status.FORBIDDEN.getStatusCode();

	}

	@Test
	public void  verifyMissingOutoundSMS() throws Exception{

		Map<String,String> payload = new HashMap<String, String>();
		payload.put("username","plivo1");
		payload.put("password","password");
		payload.put("from","8553604772");
		payload.put("text","some text");
		account = new Account(1,"password","plivo1");
		when(accountRepo.getAccount("plivo1","password")).thenReturn(account);
		when(phoneResourceUtils.authenticate("plivo1","password",account)).thenReturn(true);
		when(phoneResourceUtils.checkValid("from","to","some text")).thenReturn(null);
		when(phoneResourceUtils.checkMissing("8553604772",null,"some text")).thenReturn(new AppException(400,"from parameter missing"));
		Response response = rule.getJerseyTest().target("/outbound/sms").request().post(Entity.entity(objectMapper.writeValueAsString(payload),MediaType.APPLICATION_JSON_TYPE));
		assert response.getStatus() == Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();


	}

	@Test
	public void  verifyValidOutoundSMS() throws Exception{

		Map<String,String> payload = new HashMap<String, String>();
		payload.put("username","plivo1");
		payload.put("password","password");
		payload.put("from","8553604772");
		payload.put("to","85534545");
		payload.put("text","some text");
		account = new Account(1,"password","plivo1");
		when(accountRepo.getAccount("plivo1","password")).thenReturn(account);
		when(phoneNumberRepo.find(anyString(),anyInt())).thenReturn(new PhoneNumber());
		when(phoneResourceUtils.authenticate("plivo1","password",account)).thenReturn(true);
		when(phoneResourceUtils.aquireLock(jedis,"8553604772")).thenReturn(jedisLock);
		when(phoneResourceUtils.checkSTOP(jedis,"8553604772","85534545")).thenReturn(null);
		when(phoneResourceUtils.checkInCache(jedis,"8553604772")).thenReturn(null);
		Response response = rule.getJerseyTest().target("/outbound/sms").request().post(Entity.entity(objectMapper.writeValueAsString(payload),MediaType.APPLICATION_JSON_TYPE));
		assert response.getStatus() == Response.Status.OK.getStatusCode();

	}

	@Test
	public void  testVerifyCheckStopOutboundSMS() throws Exception {

		Map<String,String> payload = new HashMap<String, String>();
		payload.put("to","8553604772");
		payload.put("from","85536047723434");
		payload.put("text","some text");
		payload.put("username","plivo1");
		payload.put("password","password");

		account = new Account(1,"password","plivo1");
		when(accountRepo.getAccount("plivo1","password")).thenReturn(account);
		when(phoneResourceUtils.authenticate("plivo1","password",account)).thenReturn(true);
		when(phoneNumberRepo.find("85536047723434",account.getId())).thenReturn(new PhoneNumber());
		when(phoneResourceUtils.aquireLock(jedis,"85536047723434")).thenReturn(mock(JedisLock.class));
		Response response = rule.getJerseyTest().target("/outbound/sms").request().post(Entity.entity(objectMapper.writeValueAsString(payload),MediaType.APPLICATION_JSON_TYPE));
		verify(phoneResourceUtils,atLeast(1)).checkSTOP(jedis,"85536047723434", "8553604772");
	}

	@Test
	public void  testCheckStopOutboundSMS() throws Exception {

		Map<String,String> payload = new HashMap<String, String>();
		payload.put("to","8553604772");
		payload.put("from","85536047723434");
		payload.put("text","some text");
		payload.put("username","plivo1");
		payload.put("password","password");

		account = new Account(1,"password","plivo1");
		when(accountRepo.getAccount("plivo1","password")).thenReturn(account);
		when(phoneResourceUtils.authenticate("plivo1","password",account)).thenReturn(true);
		when(phoneNumberRepo.find("85536047723434",account.getId())).thenReturn(new PhoneNumber());
		when(phoneResourceUtils.aquireLock(jedis,"85536047723434")).thenReturn(jedisLock);
		when(phoneResourceUtils.checkSTOP(jedis,"85536047723434", "8553604772")).thenReturn(new AppException(412, "Some message"));
		Response response = rule.getJerseyTest().target("/outbound/sms").request().post(Entity.entity(objectMapper.writeValueAsString(payload),MediaType.APPLICATION_JSON_TYPE));
		assert response.getStatus() == Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();

	}

	@Test
	public void  testIncreaseCountOutboundSMS() throws Exception {

		Map<String,String> payload = new HashMap<String, String>();
		payload.put("to","8553604772");
		payload.put("from","85536047723434");
		payload.put("text","some text");
		payload.put("username","plivo1");
		payload.put("password","password");

		account = new Account(1,"password","plivo1");
		when(accountRepo.getAccount("plivo1","password")).thenReturn(account);
		when(phoneResourceUtils.authenticate("plivo1","password",account)).thenReturn(true);
		when(phoneNumberRepo.find("85536047723434",account.getId())).thenReturn(new PhoneNumber());
		when(phoneResourceUtils.aquireLock(jedis,"85536047723434")).thenReturn(jedisLock);
		rule.getJerseyTest().target("/outbound/sms").request().post(Entity.entity(objectMapper.writeValueAsString(payload),MediaType.APPLICATION_JSON_TYPE),Map.class);
		verify(phoneResourceUtils).increaseCount(jedis,"85536047723434");
		verify(jedisLock).release();

	}
}
