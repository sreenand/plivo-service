package integration;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import com.challur.plivoservice.PlivoServiceApplication;
import com.challur.plivoservice.Utils.PhoneResourceUtils;
import com.challur.plivoservice.configuration.PlivoServiceConfiguration;
import com.challur.plivoservice.resources.PhoneResource;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.jedis.lock.JedisLock;

import io.dropwizard.testing.junit.DropwizardAppRule;
import redis.clients.jedis.Jedis;

/**
 * Created by srinand.challur on 7/23/17.
 */
public class CommonIntegrationTest {


	private static Client client;
	private static Jedis jedis;
	private static final ObjectMapper mapper = new ObjectMapper();
	private static Map<String, String> payLoad = new HashMap<String, String>();
	private static PhoneResourceUtils phoneResourceUtils = new PhoneResourceUtils();

	private static final String FROM = "8553604772";
	private static final String TO = "4924195509198";
	private static final String USERNAME = "plivo1";
	private static final String PASSWORD = "20S0KPNOIM";
	private static final String TEXT = "Some Text";
	private static final String STOP_TEXT = "STOP";

	private static final String FROM_KEY = "from";
	private static final String TO_KEY = "to";
	private static final String USERNAME_KEY = "username";
	private static final String PASSWORD_KEY = "password";
	private static final String TEXT_KEY = "text";



	@ClassRule
	public static final DropwizardAppRule<PlivoServiceConfiguration> RULE =
			new DropwizardAppRule<PlivoServiceConfiguration>(PlivoServiceApplication.class, "integration-test.yml");

	@Before
	public void setUp(){
		if(client == null){
			client = ClientBuilder.newClient();
		}
		payLoad.put(FROM_KEY,FROM);
		payLoad.put(TO_KEY,TO);
		payLoad.put(USERNAME_KEY,USERNAME);
		payLoad.put(PASSWORD_KEY,PASSWORD);
		payLoad.put(TEXT_KEY,TEXT);
		jedis = new Jedis(RULE.getConfiguration().getRedis().getEndpoint().getHostText());
	}


	@Test
	public void testNormalFlowInboundSMS() throws JsonProcessingException{

		String payloadAsString = mapper.writeValueAsString(payLoad);
		Response response = client.target(
				"http://localhost:" + RULE.getLocalPort() + "/inbound/sms").request().post(Entity.entity(payloadAsString, MediaType.APPLICATION_JSON_TYPE));

		assert (response.getStatus() == Response.Status.OK.getStatusCode());
	}

	@Test
	public void testNormalFlowOutboundSMS() throws JsonProcessingException{

		payLoad.put(FROM_KEY,TO);
		payLoad.put(TO_KEY,FROM);

		String payloadAsString = mapper.writeValueAsString(payLoad);
		Response response = client.target(
				"http://localhost:" + RULE.getLocalPort() + "/outbound/sms").request().post(Entity.entity(payloadAsString, MediaType.APPLICATION_JSON_TYPE));

		assert (response.getStatus() == Response.Status.OK.getStatusCode());

		payLoad.put(FROM_KEY,FROM);
		payLoad.put(TO_KEY,TO);
	}

	@Test
	public void testFlowFromMissingOutboundSMS() throws JsonProcessingException{

		payLoad.remove(FROM_KEY);
		String payloadAsString = mapper.writeValueAsString(payLoad);
		Response response = client.target(
				"http://localhost:" + RULE.getLocalPort() + "/outbound/sms").request().post(Entity.entity(payloadAsString, MediaType.APPLICATION_JSON_TYPE));
		assert (response.getStatus() == Response.Status.BAD_REQUEST.getStatusCode());
		payLoad.put(FROM_KEY,FROM);
	}

	@Test
	public void testFlowToMissingOutboundSMS() throws JsonProcessingException{

		payLoad.remove(TO_KEY);
		String payloadAsString = mapper.writeValueAsString(payLoad);
		Response response = client.target(
				"http://localhost:" + RULE.getLocalPort() + "/outbound/sms").request().post(Entity.entity(payloadAsString, MediaType.APPLICATION_JSON_TYPE));
		assert (response.getStatus() == Response.Status.BAD_REQUEST.getStatusCode());
		payLoad.put(TO_KEY,TO);
	}


	@Test
	public void testFlowFromMissingInboundSMS() throws JsonProcessingException{

		payLoad.remove(FROM_KEY);
		String payloadAsString = mapper.writeValueAsString(payLoad);
		Response response = client.target(
				"http://localhost:" + RULE.getLocalPort() + "/inbound/sms").request().post(Entity.entity(payloadAsString, MediaType.APPLICATION_JSON_TYPE));
		assert (response.getStatus() == Response.Status.BAD_REQUEST.getStatusCode());
		payLoad.put(FROM_KEY,FROM);
	}

	@Test
	public void testFlowToMissingInoundSMS() throws JsonProcessingException{

		payLoad.remove(TO_KEY);
		String payloadAsString = mapper.writeValueAsString(payLoad);
		Response response = client.target(
				"http://localhost:" + RULE.getLocalPort() + "/inbound/sms").request().post(Entity.entity(payloadAsString, MediaType.APPLICATION_JSON_TYPE));
		assert (response.getStatus() == Response.Status.BAD_REQUEST.getStatusCode());
		payLoad.put(TO_KEY,TO);
	}

	@Test
	public void testPutStopInboundSMS() throws JsonProcessingException{


		payLoad.put(TEXT_KEY,STOP_TEXT);
		String payloadAsString = mapper.writeValueAsString(payLoad);
		Response response = client.target(
				"http://localhost:" + RULE.getLocalPort() + "/inbound/sms").request().post(Entity.entity(payloadAsString, MediaType.APPLICATION_JSON_TYPE));
		assert (response.getStatus() == Response.Status.OK.getStatusCode());
		String text = jedis.get(PhoneResource.STOP+"_"+FROM+"_"+TO);
		assert(text.equals(STOP_TEXT));
		payLoad.put(TEXT_KEY,TEXT);
		jedis.del(PhoneResource.STOP+"_"+FROM+"_"+TO);
	}

	@Test
	public void testCheckStopSMS() throws JsonProcessingException{

		payLoad.put(TEXT_KEY,STOP_TEXT);
		String payloadAsString = mapper.writeValueAsString(payLoad);
		client.target(
				"http://localhost:" + RULE.getLocalPort() + "/inbound/sms").request().post(Entity.entity(payloadAsString, MediaType.APPLICATION_JSON_TYPE));
		payLoad.put(FROM_KEY,TO);
		payLoad.put(TO_KEY,FROM);
		payLoad.put(TEXT_KEY,TEXT);
		payloadAsString = mapper.writeValueAsString(payLoad);
		Response response = client.target(
				"http://localhost:" + RULE.getLocalPort() + "/outbound/sms").request().post(Entity.entity(payloadAsString, MediaType.APPLICATION_JSON_TYPE));
		assert (response.getStatus() == Response.Status.NOT_ACCEPTABLE.getStatusCode());
		jedis.del(PhoneResource.STOP+"_"+FROM+"_"+TO);

		payLoad.put(FROM_KEY,FROM);
		payLoad.put(TO_KEY,TO);
	}

	@Test
	public void testCheckLockSMS() throws JsonProcessingException{

		payLoad.put(FROM_KEY,TO);
		payLoad.put(TO_KEY,FROM);
		payLoad.put(TEXT_KEY,TEXT);
		String payloadAsString = mapper.writeValueAsString(payLoad);

		JedisLock jedisLock = null;
		try {
			jedisLock = phoneResourceUtils.aquireLock(jedis, TO);
			Response response = client.target(
					"http://localhost:" + RULE.getLocalPort() + "/outbound/sms").request().post(Entity.entity(payloadAsString, MediaType.APPLICATION_JSON_TYPE));
			assert (response.getStatus() == Response.Status.PRECONDITION_FAILED.getStatusCode());
			payLoad.put(FROM_KEY,FROM);
			payLoad.put(TO_KEY,TO);
		} finally {
			jedisLock.release();
		}

	}


	@Test
	public void testCountSMS() throws JsonProcessingException{

		payLoad.put(FROM_KEY,TO);
		payLoad.put(TO_KEY,FROM);
		String payloadAsString = mapper.writeValueAsString(payLoad);

		jedis.set(PhoneResource.COUNT+"_"+TO,"50");

		Response response = client.target(
				"http://localhost:" + RULE.getLocalPort() + "/outbound/sms").request().post(Entity.entity(payloadAsString, MediaType.APPLICATION_JSON_TYPE));
		assert (response.getStatus() == Response.Status.NOT_ACCEPTABLE.getStatusCode());

		jedis.del(PhoneResource.COUNT+"_"+FROM);




	}


}
