PLIVO SERVICE APPLICATION

How to build the app

mvn install -DskipTests   - installs without running test cases

This creates a plivoservice-1.0-SNAPSHOT.jar in the target folder. 

To start this app you have to run a command java -jar target/plivoservice-1.0-SNAPSHOT.jar  server plivoService.yml

Server is the command for Dropwizard.  plivoService.yml is config file for this application.


This app is deployed on ubuntu@ec2-13-126-149-6.ap-south-1.compute.amazonaws.com

the apis can be accessed at http://ubuntu@ec2-13-126-149-6.ap-south-1.compute.amazonaws.com:8080/outbound/sms, http://ubuntu@ec2-13-126-149-6.ap-south-1.compute.amazonaws.com:8080/inbound/sms


The redis & DB are present in the same EC2 Instance.


Integration Tests config needs to be specified in integration-test.yml config

